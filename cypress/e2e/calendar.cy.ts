import * as path from 'path';

const select = (name: string): string => `[data-selector="${name}"]`;

describe('Calendar', () => {
  it('Should download ical from a session', () => {
    cy.intercept('/bobine/api/showtimes/search**', { fixture: 'anatomie' });
    cy.visit('/', {
      onBeforeLoad({ navigator }) {
        const latitude = 42;
        const longitude = 23;
        cy.stub(navigator.geolocation, 'getCurrentPosition').callsArgWith(0, { coords: { latitude, longitude } });
      },
    });
    cy.get(select('id')).type('10266');
    cy.get(select('day')).type('2024-03-14');
    cy.get(select('search')).click();
    cy.get(select('ics')).first().click();
    const downloadsFolder = Cypress.config('downloadsFolder');
    cy.readFile(path.join(downloadsFolder, '202403141245-anatomie-d-une-chute.ics')).should('exist');
  });
});
