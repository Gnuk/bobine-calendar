# Bobine calendar

Get calendar events from bobine.art

## Prerequisites

* [Node](https://nodejs.org/) LTS

## Install

```shell
npm i
```

## Launch

```shell
npm start
```

## Dev

### Tests

Unit:

```shell
npm test
```

Component:

```shell
npm run test:component
```
