import { RouteRecordRaw } from 'vue-router';

import App from './App.vue';
import Movie from './Movie.vue';

export const routes: RouteRecordRaw[] = [
  { name: 'search', path: '/', component: App },
  { name: 'movie', path: '/movie/:day/:id', component: Movie, props: true },
];
