import { inject, InjectionKey } from 'vue';

import { MovieRepository } from './domain/Movie';

export const MOVIE_REPOSITORY = Symbol('MovieRepository') as InjectionKey<MovieRepository>;

export const autowire = <T>(key: InjectionKey<T>): T => {
  const value = inject(key);
  if (value === undefined) {
    throw new Error('No provider found for:' + key.toString());
  }
  return value;
};
