import { describe, expect, it } from 'vitest';

import { Movie } from './Movie';

describe('Movie', () => {
  it('should build', () => {
    const movie = Movie.of({
      id: '123',
      title: "Le Fabuleux Destin d'Amélie Poulain",
      places: [
        {
          cinema: 'Rex',
          address: 'Here',
          sessions: [
            {
              start: new Date('2024-01-01T12:34:00.000Z'),
              end: new Date('2024-01-01T14:02:00.000Z'),
            },
          ],
        },
      ],
    });

    const session = movie.places[0].sessions[0];
    expect(movie.places[0].cinema).toBe('Rex');
    expect(movie.places[0].address).toBe('Here');
    expect(session.title).toBe("Le Fabuleux Destin d'Amélie Poulain");
    expect(session.address).toBe('Here');
    expect(session.start.iso).toBe('2024-01-01T12:34:00.000Z');
    expect(session.end.iso).toBe('2024-01-01T14:02:00.000Z');
    expect(session.slug).toEqual('202401011234-le-fabuleux-destin-d-amelie-poulain');
  });

  it('should format session date', () => {
    const start = new Date('2024-01-01T01:30:00.000');
    const end = new Date('2024-01-01T03:00:00.000');
    const movie = Movie.of({
      id: '123',
      title: "Le Fabuleux Destin d'Amélie Poulain",
      places: [
        {
          cinema: 'Rex',
          address: 'Here',
          sessions: [
            {
              start,
              end,
            },
          ],
        },
      ],
    });

    const session = movie.places[0].sessions[0];
    expect(session.start.toString()).toBe(start.toLocaleString());
    expect(session.end.toString()).toBe(end.toLocaleString());
    expect(session.start.time).toBe(start.getTime());
    expect(session.end.time).toBe(end.getTime());
  });
});
