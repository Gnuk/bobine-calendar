type Title = string;
type Address = string;
export type MovieId = string;
type Cinema = string;
type Slug = string;

interface PlaceContent {
  cinema: Cinema;
  address: Address;
  sessions: SessionContent[];
}

interface SessionContent {
  start: Date;
  end: Date;
}

class SessionDate {
  readonly iso: string;
  readonly time: number;
  private readonly human: string;
  private constructor(date: Date) {
    this.iso = date.toISOString();
    this.time = date.getTime();
    this.human = date.toLocaleString();
  }

  static of(date: Date): SessionDate {
    return new SessionDate(date);
  }

  toString(): string {
    return this.human;
  }

  get slug() {
    return this.iso.replace(/(\d+)-(\d+)-(\d+)T(\d+):(\d+).+/, '$1$2$3$4$5');
  }
}

interface MoviePlaceContent extends PlaceContent {
  title: Title;
}

interface MovieSessionContent extends SessionContent {
  title: Title;
  address: Address;
}

export class Session {
  readonly start: SessionDate;
  readonly end: SessionDate;
  private constructor(private readonly content: MovieSessionContent) {
    this.start = SessionDate.of(content.start);
    this.end = SessionDate.of(content.end);
  }

  get title(): Title {
    return this.content.title;
  }

  get address(): Address {
    return this.content.address;
  }

  get slug(): Slug {
    const titleSlug = this.title
      .toLowerCase()
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .replace(/[^a-z0-9]+/g, '-');
    return [this.start.slug, titleSlug].join('-');
  }

  static of(content: MovieSessionContent): Session {
    return new Session(content);
  }
}

class Place {
  private constructor(private content: MoviePlaceContent) {}

  get sessions(): Session[] {
    return this.content.sessions.map((session) =>
      Session.of({
        title: this.content.title,
        address: this.content.address,
        ...session,
      })
    );
  }

  get cinema(): Cinema {
    return this.content.cinema;
  }

  get address(): Address {
    return this.content.address;
  }

  static of(content: MoviePlaceContent) {
    return new Place(content);
  }
}

export class Movie {
  private constructor(private readonly content: MovieContent) {}

  get id(): MovieId {
    return this.content.id;
  }

  get title(): Title {
    return this.content.title;
  }

  get places(): Place[] {
    return this.content.places.map((place) =>
      Place.of({
        title: this.title,
        ...place,
      })
    );
  }

  static of(content: MovieContent): Movie {
    return new Movie(content);
  }
}

export interface MovieContent {
  id: MovieId;
  title: Title;
  places: PlaceContent[];
}

export interface Location {
  latitude: number;
  longitude: number;
}

export interface MovieSearch {
  id: MovieId;
  location: Location;
  start: Date;
  end: Date;
}

export interface MovieRepository {
  get(search: MovieSearch): Promise<Movie>;
}
