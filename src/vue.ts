import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';

import App from './App.vue';
import { MOVIE_REPOSITORY } from './Injection';
import { routes } from './primary/routes';
import { MovieHttp } from './secondary/MovieHttp';

const router = createRouter({
  history: createWebHistory(),
  routes,
});

createApp(App).use(router).provide(MOVIE_REPOSITORY, new MovieHttp()).mount('#app');
