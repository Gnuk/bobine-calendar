import { MovieRepository, MovieSearch, Movie } from '../domain/Movie';

const toBobineISO = (date: Date) => {
  const iso = date.toISOString();
  const [first] = iso.split('.');
  return `${first}Z`;
};

interface MovieResult {
  id: number;
  title_vo: string;
  title_vf: string;
  duration: number;
}

interface Showtime {
  showtime: string;
}

interface TheatersResult {
  address: string;
  name: string;
  showtimes: Showtime[];
}

interface SearchResult {
  movie: MovieResult;
  theaters: TheatersResult[];
}

const toMovie =
  (givenId: string) =>
  (results: SearchResult[]): Movie => {
    const [result] = results;
    if (result === undefined) {
      throw new Error('No movie found for ' + givenId);
    }
    const { duration, id, title_vf } = result.movie;
    return Movie.of({
      id: id.toString(),
      title: title_vf,
      places: result.theaters.map((theater) => ({
        address: theater.address,
        cinema: theater.name,
        sessions: theater.showtimes.map(({ showtime }) => {
          const start = new Date(showtime.split('Z')[0]);
          const end = new Date(start.getTime() + duration * 60000);
          return {
            start,
            end,
          };
        }),
      })),
    });
  };

export class MovieHttp implements MovieRepository {
  async get({ id, location, start, end }: MovieSearch): Promise<Movie> {
    const url = `/bobine/api/showtimes/search?range=10&latitude=${location.latitude}&longitude=${location.longitude}&movie_id=${id}&start=${toBobineISO(start)}&end=${toBobineISO(end)}&page=1&page_size=1`;
    const result = await fetch(url);
    const json: SearchResult[] = await result.json();
    return Promise.resolve(json).then(toMovie(id));
  }
}
